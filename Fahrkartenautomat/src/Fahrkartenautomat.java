﻿// Dominik Gutt
import java.*;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while(true)
    	{
    		Scanner tastatur = new Scanner(System.in);

    		double eingezahlterGesamtbetrag;       
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);   
    		eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur,zuZahlenderBetrag);
    		fahrkartenAusgeben(); 
    		rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);       
    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    							"vor Fahrtantritt entwerten zu lassen!\n"+
    							"Wir wünschen Ihnen eine gute Fahrt.\n\n\n");
    	}
    }

	private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag ;          // speichert den wert der für das Rückgeld berechnet wurde. Ist die Differenrz aus "eingezahlterGesamtbetrag" und "zuZahlenderBetrag". Wird auch benutzt um zu berechnen welche und wie viele Münzen ausgegeben werden.
		if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 1.995) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 0.995) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.495) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.195) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.095) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.045 )// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
		// TODO Auto-generated method stub
		
	}

	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		// TODO Auto-generated method stub
		
	}

	private static double fahrkartenBezahlen(Scanner einwurf,double zuZahlenderBetrag) {
		// TODO Auto-generated method stub
		double eingezahlterGesamtbetrag = 0.0;
	    double eingeworfeneMünze;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f€ ",(zuZahlenderBetrag - eingezahlterGesamtbetrag)); 
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = einwurf.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		return eingezahlterGesamtbetrag;
	}

	private static double fahrkartenbestellungErfassen(Scanner tastatur) {
		   double[] ticketPreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		   short[] anzahlTickets =  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		   String[] ticketInfo = {"Einzelfahrschein AB", "Enzelfahrschein BC", "Einzelfahrschein ABC", "Kurzstercke",
				   "Tageskarte AB", "Tageskarte BC", "Tageskarte ABC",
				   "Kleingruppen-Tageskarte AB", "Kleingruppen-Tageskarte BC", "Kleingruppen-tageskarte ABC" };
		   int[] auswahlnummer = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		// TODO Auto-generated method stub
	       //short anzahlTickets = 0;
	       short auswahl = 0; 
	       double einzelpreis = 100;
	       double ticketPreis = 0;
	       System.out.print("Fahrkartenbestellvorgang:\n"
	       					+ "========================\n\n");
	       
	       while(auswahl !=11) 
	       {
	    	   System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n");
	    	   for( int i = 0; i < ticketPreise.length; i++)
	    	   {
	    		   System.out.printf("%4s%5s%30s%15s%s%n", auswahlnummer[i], " ", ticketInfo[i], "", ticketPreise[i]);
	    	   }
	    	   System.out.printf("%4s%5s%30s%15s%s%n", 11, " ", "Bezahlen", "", "");

	    	   auswahl = tastatur.nextShort();
	       
	    	   while (auswahl < 1 || auswahl > 11 ) {
	    		   System.out.print("Bitte geben Sie 1, 2 oder 3 ein:");
	    		   auswahl = tastatur.nextShort();
	    	   }

	    	   if(auswahl != 11) {
		    	   einzelpreis = ticketPreise[auswahl - 1];

	    	   }
	    	   
	       
	    	   if(auswahl != 11)
	    	   {
	    		   	System.out.print("Anzahl der Tickets: ");
	    		   	anzahlTickets[auswahl-1] = tastatur.nextShort();
	    	   		while(anzahlTickets[auswahl-1] < 1 || anzahlTickets[auswahl-1] > 10)
	    	   		{
	    		   		System.out.print("Ungültige Eingabe. Versuchen Sie es nochmal und geben einen Wert zwischen 1 und 10 ein: \n");
	    		   		anzahlTickets[auswahl-1] = tastatur.nextShort();
	    	   		}
	    	   		ticketPreis += (ticketPreise[auswahl-1] * anzahlTickets[auswahl-1]);
	    	   }
	       }
		return ticketPreis;
	}
}